
package ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Fizzbuzz_QNAME = new QName("http://ws/", "fizzbuzz");
    private final static QName _FizzbuzzResponse_QNAME = new QName("http://ws/", "fizzbuzzResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Fizzbuzz }
     * 
     */
    public Fizzbuzz createFizzbuzz() {
        return new Fizzbuzz();
    }

    /**
     * Create an instance of {@link FizzbuzzResponse }
     * 
     */
    public FizzbuzzResponse createFizzbuzzResponse() {
        return new FizzbuzzResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Fizzbuzz }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "fizzbuzz")
    public JAXBElement<Fizzbuzz> createFizzbuzz(Fizzbuzz value) {
        return new JAXBElement<Fizzbuzz>(_Fizzbuzz_QNAME, Fizzbuzz.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FizzbuzzResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "fizzbuzzResponse")
    public JAXBElement<FizzbuzzResponse> createFizzbuzzResponse(FizzbuzzResponse value) {
        return new JAXBElement<FizzbuzzResponse>(_FizzbuzzResponse_QNAME, FizzbuzzResponse.class, null, value);
    }

}
